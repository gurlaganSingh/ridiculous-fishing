//
//  GameScene.swift
//  RidiculousFishing
//
//  Created by Gurlagan Bhullar on 2019-10-21.
//  Copyright © 2019 Gurlagan Bhullar. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

class GameScene: SKScene {
    
    
    var backGround: SKSpriteNode!
    var manbackGround: SKSpriteNode!
    var hook: SKSpriteNode!
    var mouseX:CGFloat = 0
    var mouseY:CGFloat = 0
    var scoreLabel : SKLabelNode!
    var score = 0
    var fish1Array:[SKSpriteNode] = [SKSpriteNode]()
    var numLoops = 0
    var hookedFishArray:[SKSpriteNode] = [SKSpriteNode]()
    var pointsFishArray:[SKSpriteNode] = [SKSpriteNode]()
    var fishMovement:[String] = [String]()
    var gameMovement:String = "up"
    var audioplayer = AVAudioPlayer()
    var fishCount:Int = 0;
    var count = 1;

       

       var tapToKillFishes = false
    //var pointsItemArray : [String] = [String]()
    var tempfish:SKTexture = SKTexture(imageNamed: "badFish")
    

    func sound(name: String) {
        do{
            audioplayer = try AVAudioPlayer(contentsOf:URL.init(fileURLWithPath: Bundle.main.path(forResource: name, ofType: "mp3")!) )
            audioplayer.prepareToPlay()
        }
        catch{
            print(error)
        }
        audioplayer.play()
    }
    override func didMove(to view: SKView) {
    // Add your hook
        self.backGround = SKSpriteNode(imageNamed: "Background")
        self.backGround.position = CGPoint(x: 375, y:-667)
        addChild(backGround)
        self.hook = SKSpriteNode(imageNamed: "hook")
        self.hook.position = CGPoint(x: 295, y: 840)
        hook.zPosition = 1;
        addChild(hook)
        sound(name: "drown");
        
        self.scoreLabel = SKLabelNode(text: "Score: \(self.score)")
        self.scoreLabel.position = CGPoint(x:140, y:1200)
        self.scoreLabel.fontColor = UIColor.red
        self.scoreLabel.fontSize = 35
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.zPosition = 1;
        
        addChild(self.scoreLabel)
        
        
        
    }
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            let touch = touches.first!
            let location = touch.location(in: self)
            let move = SKAction.move(to:CGPoint(x:location.x, y:hook.position.y), duration:0)
            let hookAnimation = SKAction.sequence(
                [move]
        )
            self.hook.run(hookAnimation)
            // tap to kill the fishes

                    if(self.tapToKillFishes == true){

                        let touch = touches.first!
                        let location = touch.location(in: self)
                        for (index, node) in self.hookedFishArray.enumerated() {

                        if node.contains(location){
                            score += 5
                            sound(name: "bullet")
                            node.removeFromParent()
                            print(self.hookedFishArray.count)
                            }
                            }

                    }

        }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

    }
    
    func moveToPosition(oldPosition: CGPoint, newPosition: CGPoint) {

        let xDistance = abs(oldPosition.x - newPosition.x)
        let yDistance = abs(oldPosition.y - newPosition.y)
        let distance = sqrt(xDistance * xDistance + yDistance * yDistance)
        let width = self.view?.frame.size.width
        let height = self.view?.frame.size.height
        let sceneDiagonal = sqrt(width! * width! + height! * height!)

        self.run(SKAction.move(to: newPosition, duration: Double(distance / sceneDiagonal / 2)))
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

        let touchLocation = touches.first!.location(in: self)
        if(self.hook.contains(touchLocation)){
            hook.position = CGPoint(x: touchLocation.x, y: hook.position.y)
           
    }
}
          
    func fish1(fishName: String) {
          // Add a cat to a static location
          let fish1 = SKSpriteNode(imageNamed: fishName)
        fish1.zPosition = 1;
        let randomXPos = CGFloat.random(in: 100 ... size.width-150)
        let randomYPos = CGFloat.random(in: -15 ... -1)
          fish1.position = CGPoint(x:randomXPos, y:randomYPos+15)
          // add the fish to the screen
          addChild(fish1)
          // add the fish to the array
          self.fish1Array.append(fish1)
        self.fishMovement.append("right")
        fishCount += 1;
      }

    func fishCreation() {
        numLoops = numLoops + 1
        if (numLoops % 80 == 0 && numLoops <= 1000)
        {
            // make a cat
            self.fish1(fishName: "fish1")
        }
          else if (numLoops % 110 == 0 && numLoops <= 1000)
          {
              // make a cat
              self.fish1(fishName: "fish2")
        }
        else if (numLoops % 150 == 0 && numLoops <= 1000)
            {
                // make a cat
                self.fish1(fishName: "badFish")
        }
            
            else if (numLoops % 130 == 0 && numLoops <= 1000)
            {
                // make a cat
                self.fish1(fishName: "coin")
        }
            else if (numLoops % 210 == 0 && numLoops <= 1000)
            {
                // make a cat
                self.fish1(fishName: "rareFish")
                //print(self.fish1(fishName: "rareFish"))
        }
        else if(numLoops == 1200){
         gameMovement = "down"
          
        }
    }
    
    func fishRightLeft(index: Int,fishNode: SKSpriteNode) {
        if(fishNode.position.x > size.width-150 ){
            fishMovement[index] = "left"
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            fishNode.run(facingLeft)
            
        }
        else if(fishNode.position.x < 100)
        {
            fishMovement[index] = "right"
             let facingRight = SKAction.scaleX(to: 1, duration: 0)
            fishNode.run(facingRight)
        }

        if(fishMovement[index] == "left")
        {
            fishNode.position.x -= 3
        }
        else
        {
            fishNode.position.x += 3

        }
    }
    
      func gameDirection() {

            if(gameMovement == "up"){

                let move2 = SKAction.move(to:CGPoint(x:375, y:4326), duration:24)

                            let backGroundAnimation = SKAction.sequence(

                            [move2]

                            )
                    self.backGround.run(backGroundAnimation)
            }
            else{
                var currentLocation = CGPoint(x:backGround.position.x,y:backGround.position.y)

                    //print(CGPoint(x:backGround.position.x,y:backGround.position.y))

                    let move2 = SKAction.move(to:CGPoint(x:375, y:-667), duration:TimeInterval((backGround.position.y+667)/204))

                    let backGroundAnimation = SKAction.sequence([move2])

                    self.backGround.run(backGroundAnimation)

                    if(backGround.position.y == -667){
                        if(self.count == 1){
                            self.tapToKillFishes = true

                                           disperseFishes()
                            print("dispersed function")

                        }
                        self.count = self.count + 1

                                      }
                        }
 }
 func disperseFishes(){
                            hookedFishArray.forEach { (node) in
                                    let randomXPos = CGFloat.random(in: 120 ... size.width-150)
                                    let randomYPos = CGFloat.random(in: 1200 ... 1800)
                                let move9 = SKAction.move(to:CGPoint(x:randomXPos, y:randomYPos), duration:10)
                                let fishAnimation = SKAction.sequence([move9])
                                node.run(fishAnimation)

                                }

         }
    override func update(_ currentTime: TimeInterval) {
        
        //print("count:\(fishCount)")
        //print("total:\(fish1Array.count)")
               if (numLoops % 50 == 0)
               {
                
        //self.score = (fishCount-fish1Array.count) * 10
                
        self.scoreLabel.text = "Score: \(self.score)"
        }
        //creating fishes
        fishCreation();
     for (index, fish1) in self.fish1Array.enumerated() {
                  if (self.hook.frame.intersects(fish1.frame) == true)
                  {
                    
                            gameMovement = "down"
                            hookedFishArray.append(fish1)
                    self.pointsFishArray.append(fish1)
                            fish1.removeFromParent()
                    self.fish1Array.remove(at:index)
                   
                    addChild(fish1);
                  }
        else
                  {
                    // moving fish in left right position
                    fishRightLeft(index: index,fishNode: fish1)
                }
        
        
        func fishUpDown(){
                    if(gameMovement == "up")
                    {
                        fish1.position.y += 2
                    }
                    else if (gameMovement == "down")
                    {
                        fish1.position.y -= 4
                    }

                }
        fishUpDown();
        
        gameDirection();
              }
        
        //Move game screen up and down
        
        
        //hook and hokked fishes location management
        for (index, hookedFish) in self.hookedFishArray.enumerated(){
            if(backGround.position.y > -667){
                hookedFish.position.x = hook.position.x
                hookedFish.position.y = hook.position.y - 55
              
                
              
            }

            //print("size\(self.hookedFishArray.count)")
        }
        for (index, hookedFish) in self.pointsFishArray.enumerated(){
            //print(hookedFish.size)
            //print(tempfish.size())
            if(hookedFish.size == tempfish.size()){
                self.score -= 15
                    print("Bad fish captured")
                
                  //print("size\(self.hookedFishArray.count)")
              }
            else{
                self.score += 10
            }
            
            
            self.pointsFishArray.remove(at: index)

    }
    

}
}
